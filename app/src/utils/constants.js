exports.mongoDbConnected = 'Successfully connected to the database';
exports.serverIsListeningOnPort = 'Server is listening on port: ';
exports.aboutTariffComparison = "This is a microservice about tariff comparison!";
exports.cannotConnectToDatabase = 'Cannot connect to database: ';
exports.lessThan4000KwhYear = 'Less than 4000 Kwh/Year';